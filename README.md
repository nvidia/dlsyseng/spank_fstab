# spank_fstab

A Slurm SPANK plugin relying on `libmount` to provide customizable per-job mounts.

```console
$ sudo make install
$ sudo ln -s /usr/local/share/fstab/fstab.conf /etc/slurm-llnl/plugstack.conf.d/fstab.conf
$ sudo systemctl restart slurmd

$ cat /etc/slurm/fstab
tmpfs /tmp tmpfs rw,nodev,nosuid,size=8G 0 0
/home/fabecassis /mnt none defaults,bind 0 0

$ srun sh -c 'findmnt /mnt ; findmnt /tmp'
TARGET SOURCE                 FSTYPE OPTIONS
/mnt   /dev/sdb2[/fabecassis] ext4   rw,relatime
TARGET SOURCE FSTYPE OPTIONS
/tmp   tmpfs  tmpfs  rw,nosuid,nodev,relatime,size=8388608k
```

## Setting the path of the spank fstab file

The plugin allows setting the path of where the plugin fstab file is to be found
with the optional `fstab` argument. E.g. the following `plugstack.conf` snippet:

```
required /path/to/spank_fstab.so fstab=/opt/slurm/etc/fstab
```

If missing, the default value is `/etc/slurm/fstab`.

## Joining the mount namespace of a job
Mount namespaces are stored in directory `/run/slurm-fstab`

```console
# To join the mount namespace of Slurm job 774
$ sudo nsenter --mount=/run/slurm-fstab/774
```

## Design
* slurmstepd is not part of the mount namespace. Which might not be what you
  want.

## Copyright and License

This project is released under the [GNU General Public License v2.0](https://gitlab.com/nvidia/dlsyseng/spank_fstab/-/blob/master/LICENSE).

This project dynamically links with [libmount](https://github.com/karelzak/util-linux/tree/v2.36.1/libmount) from the
util-linux project, libmount is licensed under the GNU Lesser General Public License version 2.1 or later.
