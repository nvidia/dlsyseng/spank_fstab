/*
 * Copyright (c) 2021, NVIDIA CORPORATION. All rights reserved.
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <linux/limits.h>
#include <linux/magic.h>

#include <sys/mount.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/wait.h>

#include <errno.h>
#include <fcntl.h>
#include <sched.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <slurm/spank.h>
#include <libmount/libmount.h>

SPANK_PLUGIN(fstab, 1)

#define log_verbose(f_, ...) \
	slurm_verbose("spank_fstab: %s: " f_, __func__, ##__VA_ARGS__)

#define log_info(f_, ...) \
	slurm_info("spank_fstab: %s: " f_, __func__, ##__VA_ARGS__)

#define log_error(f_, ...) \
	slurm_error("spank_fstab: %s: " f_, __func__, ##__VA_ARGS__)

#define log_user(f_, ...) \
	slurm_spank_log("spank_fstab: %s: " f_, __func__, ##__VA_ARGS__)

enum setns_location
{
	SLURMSTEPD_INIT,
	TASK_INIT
};

struct plugin_config {
	char runtime_path[PATH_MAX];
	char fstab_path[PATH_MAX];
	enum setns_location setns_location;
	char cwd_path[PATH_MAX];
};

static struct plugin_config config = {
	.runtime_path = { 0 },
	.fstab_path = { 0 },
	.cwd_path = { 0 },
};

static int config_parse(int ac, char **av)
{
        const char *optarg;

        memset(&config, 0, sizeof(config));

	strcpy(config.runtime_path, "/run/slurm-fstab");
	strcpy(config.fstab_path, "/etc/slurm/fstab");
	config.setns_location = SLURMSTEPD_INIT;

        for (int i = 0; i < ac; ++i) {
                if (strncmp("runtime_path=", av[i], 13) == 0) {
                        optarg = av[i] + 13;
			if (optarg[0] != '/') {
				log_error("runtime_path: path must be absolute: %s", optarg);
				return (-1);
			}

                        if (memccpy(config.runtime_path, optarg, '\0', sizeof(config.runtime_path)) == NULL) {
                                log_error("runtime_path: path too long: %s", optarg);
                                return (-1);
                        }
                } else if (strncmp("fstab=", av[i], 6) == 0) {
			optarg = av[i] + 6;
			if (optarg[0] != '/') {
				log_error("fstab_path: path must be absolute: %s", optarg);
				return (-1);
			}

                        if (memccpy(config.fstab_path, optarg, '\0', sizeof(config.fstab_path)) == NULL) {
                                log_error("fstab_path: path too long: %s", optarg);
                                return (-1);
                        }
		} else if (strncmp("setns=", av[i], 6) == 0) {
			optarg = av[i] + 6;
			if (strcmp(optarg, "slurmstepd_init") == 0)
				config.setns_location = SLURMSTEPD_INIT;
			else if (strcmp(optarg, "task_init") == 0)
				config.setns_location = TASK_INIT;
			else {
				log_error("setns: invalid value: %s", optarg);
				return (-1);
			}
                } else {
                        log_error("unknown configuration option: %s", av[i]);
                        return (-1);
                }
        }

        return (0);
}

static int is_mountpoint(const char *path)
{
	int ret;
	struct stat target, parent;
	char parent_path[PATH_MAX];

	if (stat(path, &target) < 0)
		return (-1);

	ret = snprintf(parent_path, sizeof(parent_path), "%s/..", path);
	if (ret < 0 || ret >= sizeof(parent_path))
		return (-1);

	if (stat(parent_path, &parent) < 0)
		return (-1);

	if (target.st_dev != parent.st_dev)
		return (true);

	/* root directory */
	if (target.st_ino == parent.st_ino)
		return (true);

	return (false);
}

int slurm_spank_slurmd_init(spank_t sp, int ac, char **av)
{
	int ret;
	struct statfs sfs;

	if (spank_context() != S_CTX_SLURMD)
		return (0);

	ret = config_parse(ac, av);
	if (ret < 0)
		return (-1);

	ret = mkdir(config.runtime_path, 0700);
	if (ret < 0 && errno != EEXIST) {
		log_error("couldn't mkdir %s: %m", config.runtime_path);
		return (-1);
	}

	ret = statfs(config.runtime_path, &sfs);
	if (ret < 0) {
		log_error("statfs of %s failed: %m", config.runtime_path);
		return (-1);
	}

	/* When restarting slurmd, there might still be running jobs,
	 * hence we must not touch an existing mountpoint */
	ret = is_mountpoint(config.runtime_path);
	if (ret < 0) {
		log_error("is_mountpoint(%s): %m", config.runtime_path);
		return (-1);
	}
	if (ret == true && sfs.f_type == TMPFS_MAGIC) {
		log_info("%s is already mounted", config.runtime_path);
		return (0);
	}

	ret = mount("tmpfs", config.runtime_path, "tmpfs", MS_NOSUID|MS_NODEV|MS_NOEXEC, "size=1,mode=700");
	if (ret < 0) {
		log_error("couldn't create a tmpfs on %s: %m", config.runtime_path);
		return (-1);
	}

	/* We can't have shared mount propagation, otherwise the kernel rejects
	 * bind-mounts of mount namespace files. */
	ret = mount(NULL, config.runtime_path, NULL, MS_SLAVE, NULL);
	if (ret < 0) {
		log_error("couldn't change mount propagation for %s: %m", config.runtime_path);

		ret = umount2(config.runtime_path, MNT_DETACH);
		if (ret < 0)
			log_error("couldn't unmount %s: %m", config.runtime_path);

		return (-1);
	}

	return (0);
}

/* This is needed in Slurm 20.02: https://bugs.schedmd.com/show_bug.cgi?id=7976#c12 */
int slurm_spank_slurmd_exit(spank_t sp, int ac, char **av)
{
	return (0);
}

static int get_job_mntns_path(spank_t sp, char (*path)[PATH_MAX])
{
	int ret;
	uint32_t jobid;
	spank_err_t rc;

	rc = spank_get_item(sp, S_JOB_ID, &jobid);
	if (rc != ESPANK_SUCCESS) {
		log_error("couldn't get job ID: %s", spank_strerror(rc));
		return (-1);
	}

	ret = snprintf(*path, sizeof(*path), "%s/%u", config.runtime_path, jobid);
	if (ret < 0 || ret >= sizeof(*path))
		return (-1);

	return (0);
}

static int slurm_spank_slurmstepd_init(spank_t sp, int ac, char **av)
{
	int ret;
	char job_mntns_path[PATH_MAX];
	int mntns_fd = -1;

	ret = config_parse(ac, av);
	if (ret < 0)
		return (-1);

	if (config.setns_location != SLURMSTEPD_INIT)
		return (0);

	ret = get_job_mntns_path(sp, &job_mntns_path);
	if (ret < 0)  {
		log_error("couldn't resolve the job's mount namespace path");
		return (-1);
	}

	mntns_fd = open(job_mntns_path, O_RDONLY);
	if (mntns_fd < 0) {
		log_error("couldn't open mount namespace: %m");
		return (-1);
	}

	/*
	 * slurmstepd is multi-threaded at this point, need to unshare
	 * the filesystem state of the current thread before setns
	 */
	ret = unshare(CLONE_FS);
	if (ret < 0) {
		log_error("couldn't unshare mount namespace: %m");
		return (-1);
	}

	ret = setns(mntns_fd, CLONE_NEWNS);
	if (ret < 0) {
		log_error("couldn't join mount namespace %s: %m", job_mntns_path);
		return (-1);
	}
	close(mntns_fd);

	return (0);
}

int slurm_spank_init(spank_t sp, int ac, char **av)
{
	switch (spank_context()) {
	case S_CTX_SLURMD:
		return slurm_spank_slurmd_init(sp, ac, av);
	case S_CTX_REMOTE:
		return slurm_spank_slurmstepd_init(sp, ac, av);
	default:
		return (0);
	}
}

static int send_fd(int socket, int fd)
{
	struct msghdr msg;
	struct cmsghdr *cmsg;
	char dummy = '0';
	char msgbuf[CMSG_SPACE(sizeof(int))] = { 0 };
	struct iovec iov = { .iov_base = &dummy, .iov_len = 1 };

	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = msgbuf;
	msg.msg_controllen = sizeof(msgbuf);

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	cmsg->cmsg_len = CMSG_LEN(sizeof(int));

	*((int *)CMSG_DATA(cmsg)) = fd;
	msg.msg_controllen = cmsg->cmsg_len;

	if (sendmsg(socket, &msg, MSG_NOSIGNAL) < 0) {
		log_error("sendmsg error: %m");
		return (-1);
	}

	return (0);
}

static int receive_fd(int socket)
{
	struct msghdr msg;
	struct cmsghdr *cmsg;
	char dummy = '0';
	char msgbuf[CMSG_SPACE(sizeof(int))] = { 0 };
	struct iovec iov = { .iov_base = &dummy, .iov_len = 1 };
	int fd;

	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = msgbuf;
	msg.msg_controllen = sizeof(msgbuf);

	if (recvmsg(socket, &msg, MSG_CMSG_CLOEXEC) < 0) {
		log_error("recvmsg: %m");
		return (-1);
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (cmsg == NULL) {
		log_error("empty message");
		return (-1);
	}

	fd = *((int *)CMSG_DATA(cmsg));

	return (fd);
}

/*
 * This is a paranoid check to avoid messing with the mounts of the host system.
 * Obviously, /proc could have been remounted inside the current mount namespace.
 */
static int is_mount_namespace(void)
{
	struct stat root, current;

	if (stat("/proc/1/ns/mnt", &root) < 0)
		return (-1);

	if (stat("/proc/self/ns/mnt", &current) < 0)
		return (-1);

	if (root.st_dev == current.st_dev && root.st_ino == current.st_ino)
		return (false);

	return (true);
}

static struct libmnt_context *parse_fstab(int ac, char **av)
{
	struct libmnt_context *mnt_ctx;
	struct libmnt_table *fstab;

	mnt_ctx = mnt_new_context();
	if (mnt_ctx == NULL)
		goto fail;

	fstab = mnt_new_table();
	if (fstab == NULL)
		goto fail;

	/* FIXME: mnt_table_set_parser_errcb(fstab, ...); */

	mnt_context_set_fstab(mnt_ctx, fstab);
	mnt_unref_table(fstab);

	if (mnt_table_parse_fstab(fstab, config.fstab_path) < 0)
		goto fail;

	return (mnt_ctx);

fail:
	mnt_free_context(mnt_ctx);

	return (NULL);
}

static int mount_all(struct libmnt_context *mnt_ctx)
{
	int ret, mnt_ret;
	struct libmnt_table *fstab;
	struct libmnt_iter *itr = NULL;
	struct libmnt_fs *fs;
	char buf[256];
	int rv = -1;

	if (mnt_ctx == NULL)
		return (-1);

	ret = is_mount_namespace();
	if (ret < 0) {
		log_error("is_mount_namespace(): %m");
		return (-1);
	}
	if (ret == false) {
		log_error("not inside a mount namespace");
		return (-1);
	}

	ret = mnt_context_get_fstab(mnt_ctx, &fstab);
	if (ret < 0) {
		log_error("could not get fstab from libmount context");
		goto fail;
	}

	itr = mnt_new_iter(MNT_ITER_FORWARD);
	if (itr == NULL) {
		log_error("could not initialize libmount iterator");
		goto fail;
	}

	while  (mnt_table_next_fs(fstab, itr, &fs) == 0) {
		mnt_reset_context(mnt_ctx);

		ret = mnt_context_set_fs(mnt_ctx, fs);
		if (ret < 0) {
			log_error("libmount mnt_context_set_fs failed");
			goto fail;
		}

		mnt_ret = mnt_context_mount(mnt_ctx);
		ret = mnt_context_get_excode(mnt_ctx, mnt_ret, buf, sizeof(buf));
		if (ret != MNT_EX_SUCCESS) {
			log_error("could not mount on target %s: %s", mnt_fs_get_target(fs), buf);
			goto fail;
		}
	}

	rv = 0;

fail:
	mnt_free_iter(itr);

	return (rv);
}

static int create_mount_namespace(struct libmnt_context *mnt_ctx)
{
	int ret;
	pid_t pid;
	int socket_fds[2];
	int status;
	int mntns_fd = -1;

	if (mnt_ctx == NULL)
		return (-1);

	ret = socketpair(PF_LOCAL, SOCK_SEQPACKET|SOCK_CLOEXEC, 0, socket_fds);
	if (ret < 0)
		return (-1);

	pid = fork();
	if (pid < 0) {
		close(socket_fds[0]);
		close(socket_fds[1]);
		return (-1);
	}

	if (pid == 0) { /* child */
		close(socket_fds[0]);

		ret = unshare(CLONE_NEWNS);
		if (ret < 0)
			_exit(EXIT_FAILURE);

		/* FIXME: mount propagation should be a plugin option */
		ret = mount(NULL, "/", NULL, MS_SLAVE|MS_REC, NULL);
		if (ret < 0)
			_exit(EXIT_FAILURE);

		ret = mount_all(mnt_ctx);
		if (ret < 0)
			_exit(EXIT_FAILURE);

		mntns_fd = open("/proc/self/ns/mnt", O_RDONLY);
		if (mntns_fd < 0)
			_exit(EXIT_FAILURE);

		ret = send_fd(socket_fds[1], mntns_fd);
		if (ret < 0)
			_exit(EXIT_FAILURE);
		close(socket_fds[1]);

		_exit(EXIT_SUCCESS);
	}

	close(socket_fds[1]);

	if (waitpid(pid, &status, 0) != pid) {
		log_error("could not wait for child %d", pid);
		return (-1);
	}

	if (!WIFEXITED(status) || (WEXITSTATUS(status) != 0)) {
		log_error("child %d did not terminate properly", pid);
		return (-1);
	}

	mntns_fd = receive_fd(socket_fds[0]);
	close(socket_fds[0]);
	if (mntns_fd < 0) {
		log_error("could not get mount namespace file descriptor");
		return (-1);
	}

	return (mntns_fd);
}

int slurm_spank_job_prolog(spank_t sp, int ac, char **av)
{
	int ret;
	struct libmnt_context *mnt_ctx;
	int mntns_fd = -1;
	char mntns_path[PATH_MAX];
	char saved_mntns_path[PATH_MAX];

	ret = config_parse(ac, av);
	if (ret < 0)
		return (-1);

	mnt_ctx = parse_fstab(ac, av);
	if (mnt_ctx == NULL) {
		log_error("failed to parse spank fstab file: %s", config.fstab_path);
		return (-1);
	}

	mntns_fd = create_mount_namespace(mnt_ctx);
	mnt_free_context(mnt_ctx); mnt_ctx = NULL;
	if (mntns_fd < 0) {
		log_error("couldn't create mount namespace");
		return (-1);
	}

	ret = snprintf(mntns_path, sizeof(mntns_path), "/proc/self/fd/%d", mntns_fd);
	if (ret < 0 || ret >= sizeof(mntns_path))
		return (-1);

	ret = get_job_mntns_path(sp, &saved_mntns_path);
	if (ret < 0)  {
		log_error("couldn't resolve the job's mount namespace path");
		return (-1);
	}

	/* Create empty file for the bind-mount */
	ret = open(saved_mntns_path, O_NOFOLLOW|O_CREAT, 0600);
	if (ret < 0) {
		log_error("couldn't create bind-mount target: %m");
		return (-1);
	}

	ret = mount(mntns_path, saved_mntns_path, "", MS_BIND, NULL);
	if (ret < 0) {
		log_error("couldn't save mount namespace at %s: %m", saved_mntns_path);
		return (-1);
	}

	ret = mount(NULL, saved_mntns_path, NULL, MS_UNBINDABLE, NULL);
	if (ret < 0) {
		log_error("couldn't change mount propagation for %s: %m", saved_mntns_path);
		return (-1);
	}

	return (0);
}

int slurm_spank_job_epilog(spank_t sp, int ac, char **av)
{
	int ret;
	char job_mntns_path[PATH_MAX];

	ret = config_parse(ac, av);
	if (ret < 0)
		return (-1);

	ret = get_job_mntns_path(sp, &job_mntns_path);
	if (ret < 0)  {
		log_error("couldn't resolve the job's mount namespace path");
		return (-1);
	}

	ret = umount2(job_mntns_path, MNT_DETACH);
	if (ret < 0) {
		log_error("couldn't detach job mount namespace: %m");
		return (-1);
	}

	ret = unlink(job_mntns_path);
	if (ret < 0) {
		log_error("couldn't remove file: %m");
		return (-1);
	}

	return (0);
}

int slurm_spank_task_init_privileged(spank_t sp, int ac, char **av)
{
	int ret;
	char job_mntns_path[PATH_MAX];
	char cwd_path[PATH_MAX];
	int mntns_fd = -1;

	ret = config_parse(ac, av);
	if (ret < 0)
		return (-1);

	if (config.setns_location != TASK_INIT)
		return (0);

	ret = get_job_mntns_path(sp, &job_mntns_path);
	if (ret < 0)  {
		log_error("couldn't resolve the job's mount namespace path");
		return (-1);
	}

	mntns_fd = open(job_mntns_path, O_RDONLY);
	if (mntns_fd < 0) {
		log_error("couldn't open mount namespace: %m");
		return (-1);
	}

	/*
	 * We can't use a fd here, otherwise we can end up in a
	 * directory outside of the current mount namespace.
	 */
	if (getcwd(cwd_path, PATH_MAX) == NULL) {
		log_error("couldn't get cwd path: %m");
		return (-1);
	}

	ret = setns(mntns_fd, CLONE_NEWNS);
	if (ret < 0) {
		log_error("couldn't join mount namespace %s: %m", job_mntns_path);
		return (-1);
	}
	close(mntns_fd);

	strcpy(config.cwd_path, cwd_path);

	return (0);
}

int slurm_spank_task_init(spank_t sp, int ac, char **av)
{
	int ret;

	if (config.cwd_path[0] == '\0')
		return (0);

	/* This path might not exist in the new mount namespace, so don't error out. */
	ret = chdir(config.cwd_path);
	if (ret < 0)
		log_user("couldn't chdir to previous working directory %s: %m", config.cwd_path);

	return (0);
}
